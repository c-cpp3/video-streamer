
#ifndef SERVER_H
#define SERVER_H

#include <vector>
#include <netinet/in.h>
#include <string>
#include <map>

using namespace std;

class Server {
public:
	static const int MAX_CLIENTS_DEFAULT = 2;
	static const int MESSAGE_BUFFER_SIZE = 1024;
	static const int PORT = 5000;
	static const char SERVER_SIGNAL = '\f';
	
	int run();

private:
	int maxClientCount = MAX_CLIENTS_DEFAULT;
	int currentClientCount = 0;
	fd_set socketDescriptorSet;		// Set of socket descriptors
	vector<int> clientSockets;
	vector<string> clientIPs;
	map<int, string> connectionNames;
	int masterSocket;
	int opt = 1;
	const char* acceptMessage = "Welcome.";
	const char* rejectMessage = "The server has reached its client limit and cannot accept another connection.";
	
	void onClientDisconnect(int socketIndex, sockaddr_in& address, int& addrlen);
	
	string readMessage(int socketDescriptor, bool *isDisconnectSignal);
	
	void handleIncomingMessage(int authorSocketDescriptor, const string& incomingMessage) const;
	
	void sendMessage(int currentSocketDescriptor, const string& response) const;
	
	void broadcastMessage(const string& s) const;
	
	void onClientConnect(sockaddr_in& address, int newSocket);
	
	void initializeSockets();
	
	static string decorateMessage(const string& incomingMessage, const string& authorName) ;
	
	void notifyClientsOnNewConnection(int newSocket, string newClientIP);
};


#endif //SERVER_H
