
#include "Client.h"

#include "Server.h"

#include <iostream>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstring>
#include <netdb.h>
#include <thread>
#include <chrono>

using namespace std;

void Client::run() {
	initializeSockets();
	
	thread listenThread(&Client::listenLoop, this);
	thread writeThread(&Client::writeLoop, this);
	
	listenThread.join();
	writeThread.join();
}

void Client::runAsListener() {
	initializeSockets();
	listenLoop();
}

void Client::runAsWriter() {
	initializeSockets();
	writeLoop();
}

void Client::initializeSockets() {
	// Setup a socket and connection tools
	struct hostent* host = gethostbyname(SERVER_IP);
	sockaddr_in sendSocketAddress;
	bzero((char*) &sendSocketAddress, sizeof(sendSocketAddress));
	sendSocketAddress.sin_family = AF_INET;
	sendSocketAddress.sin_addr.s_addr = inet_addr(inet_ntoa(*(struct in_addr*) *host->h_addr_list));
	sendSocketAddress.sin_port = htons(SERVER_PORT);
	clientSocketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
	
	// Try to connect...
	int status = connect(clientSocketDescriptor, (sockaddr*) &sendSocketAddress, sizeof(sendSocketAddress));
	if(status < 0) {
		toConsole("Error connecting to socket.");
		exit(1);
	}
	
	toConsole("Connection to server established.");
	
	// Be a good client and introduce yourself to the server
	send(clientSocketDescriptor, name, strlen(name), 0);
}

void Client::listenLoop() {
	char buffer[MESSAGE_BUFFER_SIZE];
	
	int reconnectionAttempts = 0;
	int bytesReceived;
	while(true) {
		bytesReceived = recv(clientSocketDescriptor, (char*) &buffer, sizeof(buffer), 0);
		
		bool errorOccurred = bytesReceived <= 0;
		if(errorOccurred) {
			if(reconnectionAttempts == ATTEMPT_LIMIT) {
				toLocalOutput("Reconnection attempt limit reached. Closing connection...");
				return;
			}
			
			if(reconnectionAttempts == 0)
				toLocalOutput("Lost connection to server.");
	
			toLocalOutput("Attempting to reconnect...");
			this_thread::sleep_for(chrono::seconds(RETRY_INTERVAL));
			reconnectionAttempts++;
			continue;
		}
		
		// Reset the counter
		reconnectionAttempts = 0;
		
		
		if(buffer[0] == Server::SERVER_SIGNAL) {
			onServerMessage(buffer);
		}
		
		
		toLocalOutput(buffer);
		
		// Clear the buffer
		memset(&buffer, 0, sizeof(buffer));
	}
}

[[noreturn]] void Client::writeLoop() {
	char buffer[MESSAGE_BUFFER_SIZE];
	
	toConsole("Write Loop started. Any input here will be sent to the server");
	
	while(true) {
		string data;
		getline(cin, data);
		
		// Clear the buffer and then copy data into it
		memset(&buffer, 0, sizeof(buffer));
		strcpy(buffer, data.c_str());
		
		// If the connection fails, a SIGPIPE is thrown, forcing the execution to stop
		// Therefore, no need to handle disconnects here  ¯\_(ツ)_/¯
		toServer(buffer);
		
		// The old way to do it
		//send(clientSocketDescriptor, (char*) &buffer, strlen(buffer), 0);
		
	}
}

Client::Client(const char* name) : name(name) {}

void Client::toServer(char* text) const {
	send(clientSocketDescriptor, text, strlen(text), 0);
}

void Client::toConsole(std::string text) {
	cout << text << endl;
}

void Client::toLocalOutput(std::string text) {
	toConsole(text);
}

void Client::onServerMessage(char* serverMessage) {
	string messageString(serverMessage);
	messageString = messageString.substr(1);
	toConsole("SERVER > " + messageString);
}
