
#ifndef CLIENT_H
#define CLIENT_H

#include <algorithm>
#include <string>

class Client {
public:
	const char* SERVER_IP = "192.168.2.100";
	const int SERVER_PORT = 5000;
	const int MESSAGE_BUFFER_SIZE = 1024;
	const int RETRY_INTERVAL = 3;
	const int ATTEMPT_LIMIT = 4;
	
	explicit Client(const char* name);
	
	virtual void run();
	
	void runAsListener();
	
	void runAsWriter();

protected:
	const char* name;
	int clientSocketDescriptor;
	
	void initializeSockets();
	
	void listenLoop();
	
	[[noreturn]] void writeLoop();
	
	void toConsole(std::string text);
	
	virtual void toLocalOutput(std::string text);
	
	void toServer(char* text) const;
	
	virtual void onServerMessage(char serverMessage[]);
};


#endif //CLIENT_H
