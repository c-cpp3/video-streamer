
#ifndef CLIENTUI_H
#define CLIENTUI_H

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include <gst/gst.h>
#include <gst/video/videooverlay.h>
#include <gtkmm.h>
#include <gtk/gtk.h>

#if defined (GDK_WINDOWING_X11)

#include <gdk/gdkx.h>

#elif defined (GDK_WINDOWING_WIN32)
#include <gdk/gdkwin32.h>
#elif defined (GDK_WINDOWING_QUARTZ)
#include <gdk/gdkquartz.h>
#endif

#include "Client.h"


class GUIClient : public Client {
private:
	GtkWidget* pMainWindow = nullptr;
	GtkWidget* pMainVideoContainer = nullptr;
	GtkWidget* pPreviewVideoContainer = nullptr;
	GtkWidget* pChatHistoryView = nullptr;
	GtkWidget* pChatTextEntry = nullptr;
	GtkWidget* pChatSendButton = nullptr;
	GtkWidget* pChatScrollWindow = nullptr;
	GtkWidget* pChatViewport = nullptr;
	
	GstElement* producerPipeline = nullptr;
	GstElement* consumerPipeline = nullptr;
	
	std::string peerIP;
	
public:
	GUIClient(const char* name);
	
	void run();
	
	void toLocalOutput(std::string text) override;
	
	void appendToChatHistory(std::string additionalText);
	
	void scrollToChatBottom();
	
	void onChatSendButtonClicked();
	
	void onTextEntryActivated();
	
	void runGUI();
	
	static void onChatSendButtonClickedStatic(GtkButton* button, gpointer* object);
	
	static void onTextEntryActivatedStatic(GtkEntry* entry, gpointer* object);
	
	void onServerMessage(char *serverMessage) override;
};


#endif //CLIENTUI_H
