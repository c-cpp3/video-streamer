
#include "Client.h"
#include "GUIClient.h"
#include "Server.h"

int main(int argc, char* argv[]) {
	
	if(argv[1][0] == 's') {
		Server server;
		server.run();
	}
	else if(argv[1][0] == 'c') {
		Client client(argv[2]);
		client.run();
	}
	else if(argv[1][0] == 'g') {
		GUIClient gc(argv[2]);
		gc.run();
	}
	
	return 0;
}
