
#include "Server.h"

#include <iostream>
#include <cstdio>
#include <cstring>            //strlen
#include <cstdlib>
#include <cerrno>
#include <ctime>            //FD_SET, FD_ISSET, FD_ZERO macros
#include <unistd.h>            //close
#include <arpa/inet.h>        //close
#include <sys/types.h>
#include <sys/socket.h>

using namespace std;

int Server::run() {
	initializeSockets();
	
	// Accept the incoming connection
	cout << "Waiting for connections..." << endl;
	
	while(true) {
		// Clear the socket set
		FD_ZERO(&socketDescriptorSet);
		
		// Add master socket to set
		FD_SET(masterSocket, &socketDescriptorSet);
		int highestDescriptorNum = masterSocket;
		
		// Add child sockets to set
		for(int i = 0; i < maxClientCount; i++) {
			int socketDescriptor = clientSockets[i];
			
			// If valid socket descriptor then add to read list
			if(socketDescriptor > 0)
				FD_SET(socketDescriptor, &socketDescriptorSet);
			
			// Highest file descriptor number - it is needed for the select function
			if(socketDescriptor > highestDescriptorNum)
				highestDescriptorNum = socketDescriptor;
		}
		
		// Wait for an activity on one of the sockets - timeout is nullptr, so it will wait indefinitely
		int activity = select(highestDescriptorNum + 1, &socketDescriptorSet, nullptr, nullptr, nullptr);
		if((activity < 0) && (errno != EINTR)) {
			printf("Error in select");
		}
		
		// If something happened on the master socket, check if it is an incoming connection or a message from an already existing one
		struct sockaddr_in address;
		int addressSize = sizeof(address);
		int newSocket;
		if(FD_ISSET(masterSocket, &socketDescriptorSet)) {
			if((newSocket = accept(masterSocket, (struct sockaddr*) &address, (socklen_t*) &addressSize)) < 0) {
				perror("accept");
				exit(EXIT_FAILURE);
			}
			
			onClientConnect(address, newSocket);
		}
		
		// Otherwise it's some IO operation on some other socket
		// Search among the sockets for incoming messages or signals
		for(int i = 0; i < clientSockets.size(); i++) {
			int currentSocketDescriptor = clientSockets[i];
			
			bool isReadyForIoOperation = FD_ISSET(currentSocketDescriptor, &socketDescriptorSet);
			if(!isReadyForIoOperation)
				continue;
			
			bool isDisconnectSignal = false;
			string incomingMessage = readMessage(currentSocketDescriptor, &isDisconnectSignal);
			
			if(isDisconnectSignal)
				onClientDisconnect(i, address, addressSize);
			else
				handleIncomingMessage(currentSocketDescriptor, incomingMessage);
			
		}
	}
	
	return 0;
}

void Server::initializeSockets() {
	clientSockets = vector<int>(MAX_CLIENTS_DEFAULT);
	clientIPs = vector<string>(MAX_CLIENTS_DEFAULT);
	
	// Initialize all clientSockets[] to 0 - not checked
	for(int i = 0; i < maxClientCount; i++) {
		clientSockets[i] = 0;
	}
	
	// Create the master socket
	masterSocket = socket(AF_INET, SOCK_STREAM, 0);
	if(masterSocket == 0) {
		perror("Socket creation failed");
		exit(EXIT_FAILURE);
	}
	
	// Set master socket to allow multiple connections
	if(setsockopt(masterSocket, SOL_SOCKET, SO_REUSEADDR, (char*) &opt, sizeof(opt)) < 0) {
		perror("Setting socket opt failed");
		exit(EXIT_FAILURE);
	}
	
	struct sockaddr_in address;
	int addressSize = sizeof(address);
	// Type of socket created
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);
	
	// Bind the socket to localhost port
	if(bind(masterSocket, (struct sockaddr*) &address, sizeof(address)) < 0) {
		perror("Binding socket failed");
		exit(EXIT_FAILURE);
	}
	printf("Listener set on port %d.\n", PORT);
	
	// Try to specify maximum of 3 pending connections for the master socket
	if(listen(masterSocket, 3) < 0) {
		perror("Error configuring masterSocket listening properties");
		exit(EXIT_FAILURE);
	}
}

void Server::handleIncomingMessage(int currentSocketDescriptor, const string& incomingMessage) const {
	const string& authorName = connectionNames.find(currentSocketDescriptor)->second;
	cout << "Incoming message [from " << authorName << "]: " << incomingMessage << endl;
	
	string finalMessage = decorateMessage(incomingMessage, authorName);
	broadcastMessage(finalMessage);
	
	// Optionally, confirm to the client that its message has been received
	//string response = "SERVER RESPONSE > Received \"" + incomingMessage + "\"";
	//sendMessage(currentSocketDescriptor, response);
}

void Server::onClientConnect(sockaddr_in& address, int newSocket) {// Inform user of socket number - used in send and receive commands
	char* newClientIP = inet_ntoa(address.sin_addr);
	printf("New connection [socket file descriptor: %d, IP: %s, port: %d] ... ", newSocket, newClientIP, ntohs(address.sin_port));
	
	if(currentClientCount == maxClientCount) {
		cout << "Max client count has been reached. The new connection will be rejected." << endl;
		if(send(newSocket, rejectMessage, strlen(rejectMessage), 0) != strlen(rejectMessage)) {
			perror("Error contacting the new connection");
		}
		return;
	}
	
	notifyClientsOnNewConnection(newSocket, newClientIP);
	
	// Send new connection greeting acceptMessage
	if(send(newSocket, acceptMessage, strlen(acceptMessage), 0) != strlen(acceptMessage)) {
		perror("Error sending info about the new connection");
	}
	
	cout << "Welcoming message sent successfully." << endl;
	
	bool gotDisconnected = false;
	string newConnectionName = readMessage(newSocket, &gotDisconnected);
	
	// Just in case the client gets disconnected before even sending its name
	if(gotDisconnected)
		return;
	
	// Get the client's name
	cout << "The new connection's name: " << newConnectionName << endl;
	connectionNames[newSocket] = newConnectionName;
	
	// Add new socket to the socket vector
	for(int i = 0; i < maxClientCount; i++) {
		// If position is empty
		if(clientSockets[i] == 0) {
			clientSockets[i] = newSocket;
			clientIPs[i] = newClientIP;
			
			printf("Adding to list of sockets as %d\n", i);
			
			break;
		}
	}
	
	currentClientCount++;
}

void Server::notifyClientsOnNewConnection(int newSocket, string newClientIP) {
	
	// If there is not another client connected to the server, there is nothing to do
	if(currentClientCount == 0)
		return;
	
	
	// Search for the already connected client
	int indexOfFirstClient;
	for(int i = 0; i < clientSockets.size(); i++) {
		if(clientSockets[i] != 0) {
			indexOfFirstClient = i;
			continue;
		}
	}
	
	int firstClientSocketDescriptor = clientSockets[indexOfFirstClient];
	string firstClientIP = clientIPs[indexOfFirstClient];
	
	
	// Add SERVER_SIGNAL ('/f') at the front of the messages to let the clients know it's a message from the server - sloppy, I know
	string messageToFirstClient = SERVER_SIGNAL + newClientIP;
	const char* messageToFirstClientCstr = messageToFirstClient.c_str();
	
	string messageToSecondClient = SERVER_SIGNAL + firstClientIP;
	const char* messageToSecondClientCstr = messageToSecondClient.c_str();
	
	if(send(newSocket, messageToSecondClientCstr, strlen(messageToSecondClientCstr), 0) != strlen(
			messageToSecondClientCstr)) {
		perror("Error contacting the new connection");
	}
	
	if(send(firstClientSocketDescriptor, messageToFirstClientCstr, strlen(messageToFirstClientCstr), 0) != strlen(messageToFirstClientCstr)) {
		perror("Error contacting the new connection");
	}
}

void Server::onClientDisconnect(int socketIndex, sockaddr_in& address, int& addrlen) {
	// Fetch the disconnected client's info
	int socket = clientSockets[socketIndex];
	getpeername(socket, (struct sockaddr*) &address, (socklen_t*) &addrlen);
	
	printf("Client disconnected [IP %s , port %d].\n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));
	
	// Close the socket and mark as 0 to enable the spot for reuse
	close(socket);
	clientSockets[socketIndex] = 0;
	
	connectionNames.erase(socket);
	
	currentClientCount--;
}

string Server::readMessage(int socketDescriptor, bool* isDisconnectSignal) {
	int messageSize;
	char buffer[1024];
	
	messageSize = read(socketDescriptor, buffer, MESSAGE_BUFFER_SIZE - 1);
	
	if(messageSize <= 0) {
		*isDisconnectSignal = true;
		return string();
	}
	
	// Remove line feed characters from the end of the buffer - reduce the messageSize variable accordingly
	int i = messageSize - 1;
	while(buffer[i] == '\r' || buffer[i] == '\n') {
		buffer[i--] = '\0';
	}
	messageSize = i + 1;
	
	return string(buffer, buffer + messageSize);
}

void Server::sendMessage(int currentSocketDescriptor, const string& response) const {
	const char* response_cstr = response.c_str();
	send(currentSocketDescriptor, response_cstr, strlen(response_cstr), 0);
}

void Server::broadcastMessage(const string& s) const {
	for(int i = 0; i < maxClientCount; i++) {
		int currentSocketDescriptor = clientSockets[i];
		
		if(currentSocketDescriptor == 0)
			continue;
		
		sendMessage(currentSocketDescriptor, s);
	}
}

string Server::decorateMessage(const string& incomingMessage, const string& authorName) {
	time_t now = time(nullptr);
	tm* ltm = localtime(&now);
	int hour = ltm->tm_hour;
	int minute = ltm->tm_min;
	
	string hourSupplement = (hour < 10) ? "0" : "";
	string minuteSupplement = (minute < 10) ? "0" : "";
	
	string decoratedString = authorName
			+ " [" + hourSupplement + to_string(hour) + ":" + minuteSupplement + to_string(minute) + "]"
			+ "\r\n" + incomingMessage
		//	+ "\r\n" + "──────────────────────────"
			;
	return decoratedString;
}
