// GUIClient with C bindings


#include "GUIClient.h"

#include "streaming/PipelineBuilder.h"
#include "streaming/monitors/Monitor.h"
#include "streaming/monitors/MonitorInfoProvider.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <sstream>

using namespace std;

void GUIClient::run() {
	Client::initializeSockets();
	
	std::thread listenThread(&GUIClient::listenLoop, this);
	
	runGUI();
	
	exit(0);
}

void GUIClient::runGUI() {
	gtk_init(nullptr, nullptr);
	gst_init(nullptr, nullptr);
	
	
	GtkBuilder* gtkBuilder = gtk_builder_new();
	gtk_builder_add_from_file(gtkBuilder, "../ClientUI.glade", nullptr);
	
	
	pMainWindow = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "mainWindow"));
	pMainVideoContainer = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "mainVideoContainer"));
	pPreviewVideoContainer = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "previewVideoContainer"));
	pChatTextEntry = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "chatTextEntry"));
	pChatSendButton = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "chatSendButton"));
	pChatHistoryView = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "chatTextView"));
	pChatScrollWindow = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "chatScrollWindow"));
	pChatViewport = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "chatViewport"));
	
	
	g_signal_connect(G_OBJECT(pChatSendButton), "clicked", G_CALLBACK(onChatSendButtonClickedStatic), this);
	g_signal_connect(G_OBJECT(pChatTextEntry), "activate", G_CALLBACK(onTextEntryActivatedStatic), this);
	
	
	
	
	
	
	
	//=== Run consumer pipeline =======================================================================================================================
	
	string mainSinkName = "GTKSINK";
	string consumerDescription = PipelineBuilder::buildVideoConsumerPipeline(mainSinkName);
	consumerPipeline = gst_parse_launch(consumerDescription.c_str(), nullptr);
	
	GstElement* mainGtkSink = gst_bin_get_by_name((GstBin*)consumerPipeline, mainSinkName.c_str());
	
	
	// Get the GTK widget that GStreamer uses as a sink and add it to the application's window
	GtkWidget* mainVideoContainer = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "mainVideoContainer"));
	GtkWidget* mainVideoArea;
	g_object_get(mainGtkSink, "widget", &mainVideoArea, nullptr);
	gtk_container_add(GTK_CONTAINER (mainVideoContainer), mainVideoArea);
	gtk_widget_realize(mainVideoArea);
	
	// Start the pipeline
	gst_element_set_state (consumerPipeline, GST_STATE_PLAYING);
	
	
	
	
	//=== Run producer pipeline =======================================================================================================================
	
	Monitor selectedMonitor = MonitorInfoProvider::getAvailableMonitor(1);

	string previewSinkName = "GTKSINK";
	string producerDescription = PipelineBuilder::buildScreenProducerPipeline(selectedMonitor, "192.168.2.101", previewSinkName);
	producerPipeline = gst_parse_launch(producerDescription.c_str(), nullptr);

	GstElement* previewGtkSink = gst_bin_get_by_name((GstBin*)producerPipeline, previewSinkName.c_str());

	// Get the GTK widget that GStreamer uses as a sink and add it to the application's window
	GtkWidget* previewVideoContainer = GTK_WIDGET(gtk_builder_get_object(gtkBuilder, "previewVideoContainer"));
	GtkWidget* previewVideoArea;
	g_object_get(previewGtkSink, "widget", &previewVideoArea, nullptr);
	gtk_container_add(GTK_CONTAINER (previewVideoContainer), previewVideoArea);
	gtk_widget_realize(previewVideoArea);

	// Start the pipeline
	gst_element_set_state (producerPipeline, GST_STATE_PLAYING);
	
	
	
	
	
	
	
	
	
	
	
	
	gtk_widget_show_all(pMainWindow);
	gtk_main();
	
}

void GUIClient::onTextEntryActivated() {
	onChatSendButtonClicked();
}

void GUIClient::onChatSendButtonClickedStatic(GtkButton* button, gpointer* object) {
	reinterpret_cast<GUIClient*>(object)->onChatSendButtonClicked();
}

void GUIClient::onChatSendButtonClicked() {
	const gchar* text = gtk_entry_get_text(reinterpret_cast<GtkEntry*>(pChatTextEntry));
	
	if(strlen(text) == 0)
		return;
	
	//appendToChatHistory(text);
	char buffer[Client::MESSAGE_BUFFER_SIZE];
	memset(&buffer, 0, Client::MESSAGE_BUFFER_SIZE);
	strcpy(buffer, text);
	
	Client::toServer(buffer);
	
	gtk_entry_set_text(reinterpret_cast<GtkEntry*>(pChatTextEntry), "");
}

void GUIClient::appendToChatHistory(string additionalText) {
	auto* textView = reinterpret_cast<GtkTextView*>(pChatHistoryView);
	GtkTextBuffer* textViewBuffer = gtk_text_view_get_buffer(textView);
	
	GtkTextIter endIter;
	gtk_text_buffer_get_end_iter(textViewBuffer, &endIter);
	
	const char* additionalTextCstr = additionalText.c_str();
	gtk_text_buffer_insert(textViewBuffer, &endIter, additionalTextCstr, strlen(additionalTextCstr));
	
	
	// Find a way to refresh the chat history size before executing this
	scrollToChatBottom();
}

void GUIClient::scrollToChatBottom() {
	// https://stackoverflow.com/questions/4479005/gtk-scrolled-window-scroll-to-the-bottom
	
	try {
		GtkAdjustment* pAdjustment = gtk_scrolled_window_get_vadjustment(reinterpret_cast<GtkScrolledWindow*>(pChatScrollWindow));
		gdouble upper = gtk_adjustment_get_upper(pAdjustment);
		gtk_adjustment_set_value(pAdjustment, upper);
	}
	catch(...) {
		// Simply ignore this
	}
}

void GUIClient::toLocalOutput(std::string text) {
	if(pChatHistoryView != nullptr)
		appendToChatHistory(text + string("\r\n\r\n"));
}

GUIClient::GUIClient(const char* name) : Client(name) {}

void GUIClient::onTextEntryActivatedStatic(GtkEntry* entry, gpointer* object) {
	onChatSendButtonClickedStatic(nullptr, object);
}

void GUIClient::onServerMessage(char* serverMessage) {
	Client::onServerMessage(serverMessage);
	
	string serverMessageString(serverMessage);
	
	// The message received by the server is the IP of another client
	peerIP = serverMessageString;
	
}

