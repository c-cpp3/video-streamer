
#include "PipelineBuilder.h"


#include <sstream>

string PipelineBuilder::buildScreenProducerPipeline(const Monitor& selectedMonitor, const string& receiverIp, const string& previewSinkName, int port) {
	// 15 FPS with a key frame every 18-20 frames and 4096 bps seems to be a sweet spot
	
	// The output should be something like this:
	/*
	ximagesrc startx=1 starty=1 endx=1365 endy=767 use-damage=0 ! tee name=original_pipeline ! queue ! videoscale ! ...
	video/x-raw, width=819, height=460 ! timeoverlay text="Time elapsed:" valignment=bottom halignment=right font-desc="Sans, 12" shaded-background=yes ! ...
	textoverlay text="Stream preview" valignment=top halignment=right font-desc="Sans, 12" shaded-background=yes ! ximagesink  original_pipeline. ! ...
	video/x-raw, framerate=20/1 ! videoscale ! video/x-raw, width=1366, height=768 ! videoconvert ! ...
	x264enc bitrate=2048 interlaced=0 b-adapt=1 key-int-max=20 tune=zerolatency ! rtph264pay ! udpsink host=192.168.2.100 port=5070
	*/
	
	bool damage = false;
	int marginSize = 1;
	stringstream ximagesrc;
	ximagesrc << "ximagesrc"
			  << SPACE << "startx=" << selectedMonitor.getStartX() + marginSize
			  << SPACE << "starty=" << selectedMonitor.getStartY() + marginSize
			  << SPACE << "endx=" << selectedMonitor.getEndX() - marginSize
			  << SPACE << "endy=" << selectedMonitor.getEndY() - marginSize
			  << SPACE << "use-damage=" << damage;
	
	
	// A tee to a preview window on the producer
	stringstream previewTee = createPreviewTee("screen", previewSinkName);
	
	
	int framerate = 15;
	stringstream framerateCap;
	framerateCap << "video/x-raw,"
				 << SPACE << "framerate=" << framerate << "/1";
	
	
	// The resolution quality of the stream
	double resolutionQualityFactor = 1.0;		// ranges from 0.0 to 1.0
	int newWidth = static_cast<int>(selectedMonitor.getWidth() * resolutionQualityFactor);
	int newHeight = static_cast<int>(selectedMonitor.getHeight() * resolutionQualityFactor);
	stringstream videoscale;
	videoscale << "videoscale"
			   << LINK
			   << "video/x-raw,"
			   << SPACE << "width=" << newWidth << ","
			   << SPACE << "height=" << newHeight;
	
	
	int bitrate = 4096;
	bool interlaced = false;
	bool bAdapt = true;
	int framesBetweenKeyFrames = framerate * 1.2;		// Every 1.2 seconds (at most) a key frame will be inserted
	stringstream x264encode;
	x264encode << "x264enc"
			   << SPACE << "bitrate=" << bitrate
			   << SPACE << "interlaced=" << interlaced
			   << SPACE << "b-adapt=" << bAdapt
			   << SPACE << "key-int-max=" << framesBetweenKeyFrames
			   << SPACE << "tune=" << "zerolatency";
	
	
	stringstream udpsink;
	udpsink << "udpsink"
			<< SPACE << "host=\"" << receiverIp << "\""
			<< SPACE << "port=" << port;
	
	
	stringstream pipeline;
	pipeline << ximagesrc.str()
			 << LINK << previewTee.str()
			 << LINK << framerateCap.str()		// consider QUEUE here?
			 << LINK << videoscale.str()
			 << LINK << "videoconvert"
			 << LINK << x264encode.str()
			 //<< LINK << "h264parse"		// is parsing needed?
			 << LINK << "rtph264pay"
			 << LINK << udpsink.str();
	
	return pipeline.str();
}

string PipelineBuilder::buildCameraProducerPipeline(const string& receiverIp, const string& previewSinkName, int port) {
	// The output should be something like this:
	/*
	v4l2src ! videoscale ! video/x-raw,framerate=30/1,width=600,height=400 ! tee name=original_pipeline ! queue ! videoconvert ! ..
	timeoverlay text="Time elapsed:" valignment=bottom halignment=right font-desc="Sans, 12" shaded-background=yes ! ...
	textoverlay text="Camera preview" valignment=top halignment=right font-desc="Sans, 12" shaded-background=yes ! ximagesink  original_pipeline. ! ...
	videoconvert ! x264enc bitrate=1024 b-adapt=1 key-int-max=6 tune=zerolatency ! rtph264pay ! udpsink host="192.168.2.100" port=5060
	*/
	
	stringstream v4l2src;
	v4l2src << "v4l2src";
	
	// A tee to a preview window on the producer
	stringstream previewTee = createPreviewTee("camera", previewSinkName);
	
	
	int framerate = 30;
	stringstream videoscale;
	videoscale << "video/x-raw,"
			   << SPACE << "framerate=" << framerate << "/1,"
			   << SPACE << "width=" << 600 << ","
			   << SPACE << "height=" << 400;
	
	
	int bitrate = 4096;
	//int framesBetweenKeyFrames = framerate / 5;		// This guarantees one keyframe every 1/5 seconds
	//framesBetweenKeyFrames = max(framesBetweenKeyFrames, 1);
	int framesBetweenKeyFrames = 3;						// This guarantees one keyframe every 4 frames
	stringstream x264encode;
	x264encode << "x264enc"
			   << SPACE << "bitrate=" << bitrate
			   << SPACE << "key-int-max=" << framesBetweenKeyFrames
			   << SPACE << "tune=" << "zerolatency";
	
	
	stringstream udpsink;
	udpsink << "udpsink"
			<< SPACE << "host=\"" << receiverIp << "\""
			<< SPACE << "port=" << port;
	
	
	stringstream pipeline;
	pipeline << v4l2src.str()
			 << LINK << previewTee.str()
			 << LINK << videoscale.str()		// consider QUEUE here?
			 << LINK << "videoconvert"
			 << LINK << x264encode.str()
			 //<< LINK << "h264parse"		// is parsing needed?
			 << LINK << "rtph264pay"
			 << LINK << udpsink.str();
	
	return pipeline.str();
}

stringstream PipelineBuilder::createPreviewTee(const Monitor& selectedMonitor, const string& previewName, const string& previewSinkName, const string& pipelineName) {
	return createPreviewTee(previewName, previewSinkName, selectedMonitor.getDimensions(), pipelineName);
}

stringstream PipelineBuilder::createPreviewTee(const string& previewName, const string& previewSinkName, Coordinates dimensions, const string& pipelineName) {
	
	// Scale of the preview window relative to the screen being shared
	double scaleFactor = 0.6;
	stringstream videoScale;
	videoScale << "videoscale"
			   << LINK
			   << "video/x-raw,"
			   << SPACE << "width=" << static_cast<int>(dimensions.getX() * scaleFactor) << ","
			   << SPACE << "height=" << static_cast<int>(dimensions.getY() * scaleFactor);
	
	
	string timeoverlayLabel = "\"Time elapsed:\"";
	stringstream timeoverlay;
	timeoverlay << "timeoverlay"
				<< SPACE << "text=" << timeoverlayLabel
				<< SPACE << "valignment=" << "bottom"
				<< SPACE << "halignment=" << "right"
				<< SPACE << "font-desc=" << "\"Sans, 12\""
				<< SPACE << "shaded-background=" << "yes";
	
	
	string textOverlayLabel = previewName;
	stringstream textoverlay;
	textoverlay << "textoverlay"
				<< SPACE << "text=\"" << textOverlayLabel << "\""
				<< SPACE << "valignment=" << "top"
				<< SPACE << "halignment=" << "right"
				<< SPACE << "font-desc=" << "\"Sans, 12\""
				<< SPACE << "shaded-background=" << "yes";
	
	
	stringstream gtksink;
	gtksink << "gtksink"
			<< SPACE << "name=" << previewSinkName;
	
	stringstream teeToGtksink;
	teeToGtksink << "tee name=" << pipelineName
				 << QUEUE << videoScale.str()	// The QUEUE is essential here - without it the preview would freeze
				 //<< LINK << timeoverlay.str()
				 //<< LINK << textoverlay.str()
				 << LINK << gtksink.str()
				 << SPACE << pipelineName << ".";
	return teeToGtksink;
}

string PipelineBuilder::buildVideoConsumerPipeline(const string& sinkName, int port, const string& ip) {
	stringstream udpsrc;
	udpsrc << "udpsrc"
		   << SPACE << "port=" << port
		   << SPACE << "caps=" << "\"application/x-rtp\"" << SPACE;
	
	if(ip != IP_DEFAULT_FOR_CONSUMER)
		udpsrc << SPACE << "address=\"" << ip << "\"";		// If the IP is not the default, on top of the above, add this
	
	stringstream gtksink;
	gtksink << "gtksink"
			<< SPACE << "name=" << sinkName;
	
	stringstream pipeline;
	pipeline << udpsrc.str()
			 << LINK << "rtph264depay"
			 //<< LINK << "h264parse"		// again - is parsing needed?
			 << LINK << "avdec_h264"
			 << LINK << "videoconvert"
			 << LINK << gtksink.str();
	
	return pipeline.str();
}

string PipelineBuilder::buildAudioProducerPipeline(const string& receiverIp, int port) {
	
	bool echoCanceling = true;
	int echoSuppressionLevel = 2;	// Ranges from 0 to 2
	bool noiseSuppression = true;
	int noiseSuppressionLevel = 3;	// Ranges from 0 to 3
	bool voiceDetection = false;
	bool highPassFiltering = false;
	stringstream webrtcdsp;
	webrtcdsp << "webrtcdsp"
			  << SPACE << "echo-cancel=" << echoCanceling
			  << SPACE << "echo-suppression-level=" << echoSuppressionLevel
			  << SPACE << "noise-suppression=" << noiseSuppression
			  << SPACE << "noise-suppression-level=" << noiseSuppressionLevel
			  << SPACE << "voice-detection=" << voiceDetection
			  << SPACE << "high-pass-filter=" << highPassFiltering;
	
	stringstream udpsink;
	udpsink << "udpsink"
			<< SPACE << "host=\"" << receiverIp << "\""
			<< SPACE << "port=" << port;
	
	stringstream pipeline;
	pipeline << "alsasrc"
			 << LINK << webrtcdsp.str()
			 << LINK << "webrtcechoprobe"
			 << LINK << "audioconvert"
			 << LINK << "rtpL24pay"
			 << LINK << udpsink.str();
	
	return pipeline.str();
}

string PipelineBuilder::buildAudioConsumerPipeline(int port, const string& ip) {
	stringstream udpsrc;
	udpsrc << "udpsrc"
		   << SPACE << "port=" << port
		   << SPACE << "caps=" << "\"application/x-rtp,channels=(int)2,format=(string)S16LE,media=(string)audio,payload=(int)96,clock-rate=(int)44100,encoding-name=(string)L24\""
		   << SPACE;
	
	if(ip != IP_DEFAULT_FOR_CONSUMER)
		udpsrc << SPACE << "address=\"" << ip << "\"" << SPACE;		// If the IP is not the default, on top of the above, add this
	
	stringstream pipeline;
	pipeline << udpsrc.str()
			 << LINK << "rtpL24depay"
			 << LINK << "audioconvert"
			 << LINK << "pulsesink";
	
	return pipeline.str();
}
