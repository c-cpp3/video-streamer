
#ifndef PIPELINEBUILDER_H
#define PIPELINEBUILDER_H

#include <string>
#include "monitors/Monitor.h"
#include "monitors/Coordinates.h"

using namespace std;

class PipelineBuilder {
private:
	inline static const string LINK = " ! ";
	inline static const string QUEUE = " ! queue ! ";
	inline static const string SPACE = " ";
	
public:
	inline static const string IP_LOOPBACK = "127.0.0.1";
	inline static const string IP_DEFAULT_FOR_CONSUMER = "0.0.0.0";
	
	inline static const int GENERIC_PORT_DEFAULT = 5004;	// This is the port GStreamer uses by default
	inline static const int VIDEO_PORT_DEFAULT = 5005;
	inline static const int AUDIO_PORT_DEFAULT = 5006;
	
	inline static const int DEFAULT_PREVIEW_WIDTH = 384;
	inline static const int DEFAULT_PREVIEW_HEIGHT = 216;
	
	
	static string buildScreenProducerPipeline(const Monitor& selectedMonitor, const string& receiverIp, const string& previewSinkName, int port=VIDEO_PORT_DEFAULT);
	
	static string buildCameraProducerPipeline(const string& receiverIp, const string& previewSinkName, int port=VIDEO_PORT_DEFAULT);
	
	static string buildAudioProducerPipeline(const string& receiverIp, int port=AUDIO_PORT_DEFAULT);
	
	[[deprecated]] static stringstream createPreviewTee(const Monitor& selectedMonitor, const string& previewName, const string& previewSinkName, const string& pipelineName = "original_pipeline");
	
	static stringstream createPreviewTee(const string& previewName, const string& previewSinkName, Coordinates dimensions=getDefaultPreviewCoordinates(), const string& pipelineName = "original_pipeline");
	
	static string buildVideoConsumerPipeline(const string& sinkName, int port= VIDEO_PORT_DEFAULT, const string& ip= IP_DEFAULT_FOR_CONSUMER);
	
	static string buildAudioConsumerPipeline(int port= AUDIO_PORT_DEFAULT, const string& ip= IP_DEFAULT_FOR_CONSUMER);
	
	static Coordinates getDefaultPreviewCoordinates() {
		return Coordinates(DEFAULT_PREVIEW_WIDTH, DEFAULT_PREVIEW_HEIGHT);
	}
	
};


#endif //PIPELINEBUILDER_H
