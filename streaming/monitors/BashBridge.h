
#ifndef BASHBRIDGE_H
#define BASHBRIDGE_H


#include <string>
#include <vector>

using namespace std;

class BashBridge {
public:
	static string executeSquashed(const char* command);
	static vector<string> execute(const char* command);
};


#endif //BASHBRIDGE_H
