
#ifndef MONITOR_H
#define MONITOR_H

#include <string>
#include "Coordinates.h"

using namespace std;

class Monitor {
private:
	string name;
	bool isPrimary;
	Coordinates startingPoint;
	Coordinates dimensions;
public:
	Monitor();
	
	Monitor(const string& name, bool isPrimary, int x, int y, int width, int height);
	
	const string& getName() const;
	
	bool isMonitorPrimary() const;
	
	int getStartX() const;
	
	int getStartY() const;
	
	int getEndX() const;
	
	int getEndY() const;
	
	int getWidth() const;
	
	int getHeight() const;
	
	const Coordinates& getDimensions() const;
	
	string toString() const;
};


#endif //MONITOR_H
