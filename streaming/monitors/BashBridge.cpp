
#include "BashBridge.h"

#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>


string BashBridge::executeSquashed(const char* command) {
	array<char, 128> buffer;
	string result;
	unique_ptr<FILE, decltype(&pclose)> pipe(popen(command, "r"), pclose);
	
	if (!pipe) {
		throw runtime_error("popen() failed!");
	}
	
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		result += buffer.data();
	}
	
	return result;
}

vector<string> BashBridge::execute(const char* command) {
	vector<string> outputLines;
	array<char, 128> buffer;
	unique_ptr<FILE, decltype(&pclose)> pipe(popen(command, "r"), pclose);
	
	if (!pipe) {
		throw runtime_error("popen() failed!");
	}
	
	while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
		outputLines.emplace_back(buffer.data());
	}
	
	return outputLines;
}

