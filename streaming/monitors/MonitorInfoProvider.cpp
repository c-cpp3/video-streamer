
#include <sstream>
#include "MonitorInfoProvider.h"


#include "BashBridge.h"

using namespace std;

Monitor MonitorInfoProvider::getAvailableMonitor(int ordinal) {
	vector<string> monitorInfoLines = BashBridge::execute("xrandr | grep \" connected \"");
	
	string monitorName;
	int x, y, width, height;
	bool isPrimary;
	
	parseInfoLine(monitorInfoLines[ordinal - 1], monitorName, isPrimary, x, y, width, height);
	
	return Monitor(monitorName, isPrimary, x, y, width, height);
}

vector<Monitor> MonitorInfoProvider::getAvailableMonitors() {
	vector<Monitor> monitors;
	vector<string> monitorInfoLines = BashBridge::execute("xrandr | grep \" connected \"");
	
	string monitorName;
	int x, y, width, height;
	bool isPrimary;
	Monitor currentMonitor;
	
	for(const string& infoLine : monitorInfoLines) {
		parseInfoLine(infoLine, monitorName, isPrimary, x, y, width, height);
		
		currentMonitor = Monitor(monitorName, isPrimary, x, y, width, height);
		
		if(isPrimary) {
			monitors.insert(monitors.begin(), currentMonitor);
		}
		else {
			monitors.emplace_back(currentMonitor);
		}
	}
	
	return monitors;
}

void MonitorInfoProvider::parseInfoLine(const string& infoLine, string& name, bool& isPrimary, int& x, int& y, int& width, int& height) {
	
	/*	As an example, a line could be:
	 *
	 *		"LVDS1 connected primary 1366x768+0+1080 (normal left inverted right x axis y axis) 280mm x 160mm\n"
	 *
	 *	or
	 *
	 *		"DP1 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 530mm x 300mm\n"
	 *
	 */
	
	vector<string> lineTokens;
	
	istringstream lineStream = istringstream{infoLine};
	string currentToken;
	
	// The first token is the name
	lineStream >> name;
	
	// The second token should be "connected" - which can be ignored
	lineStream >> currentToken;
	
	// The third token is "primary" for the primary monitor - if it's not "primary", it's the monitor "topology"
	lineStream >> currentToken;
	isPrimary = false;
	if(currentToken == "primary") {
		isPrimary = true;
		lineStream >> currentToken;
	}
	
	// And last, parse the monitor topology string
	int lastSeparator = 0;
	string xString, yString, widthString, heightString;
	for(int i = 0; i < currentToken.length(); i++) {
		if(currentToken[i] == 'x') {
			widthString = currentToken.substr(0, i);
			lastSeparator = i;
		}
		else if(currentToken[i] == '+') {
			if(currentToken[lastSeparator] == 'x') {
				heightString = currentToken.substr(lastSeparator + 1, i - lastSeparator - 1);
				lastSeparator = i;
			}
			else {
				xString = currentToken.substr(lastSeparator + 1, i - lastSeparator - 1);
				lastSeparator = i;
				break;
			}
		}
	}
	yString = currentToken.substr(lastSeparator + 1, currentToken.length() -lastSeparator);
	
	width = atoi(widthString.c_str());
	height = atoi(heightString.c_str());
	x = atoi(xString.c_str());
	y = atoi(yString.c_str());
	
}
