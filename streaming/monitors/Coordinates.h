
#ifndef COORDINATES_H
#define COORDINATES_H


class Coordinates {
private:
	int x, y;
public:
	Coordinates(int x, int y);
	
	Coordinates();
	
	Coordinates operator+(const Coordinates& rhs) const;
	
	int getX() const;
	
	int getY() const;
	
};


#endif //COORDINATES_H
