
#ifndef MONITORINFOPROVIDER_H
#define MONITORINFOPROVIDER_H


#include <vector>
#include <string>

#include "Monitor.h"

using namespace std;

class MonitorInfoProvider {
private:
	static void parseInfoLine(const string& infoLine, string& name, bool& isPrimary, int& x, int& y, int& width, int& height);
public:
	static Monitor getAvailableMonitor(int ordinal);
	static std::vector<Monitor> getAvailableMonitors();
};


#endif //MONITORINFOPROVIDER_H
