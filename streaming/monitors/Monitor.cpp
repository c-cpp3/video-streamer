
#include <sstream>
#include "Monitor.h"

Monitor::Monitor() {}

Monitor::Monitor(const string& name, bool isPrimary, int x, int y, int width, int height) : name(name),
																							isPrimary(isPrimary),
																							startingPoint(x, y),
																							dimensions(width, height) {}

const string& Monitor::getName() const {
	return name;
}

bool Monitor::isMonitorPrimary() const {
	return isPrimary;
}

int Monitor::getStartX() const {
	return startingPoint.getX();
}

int Monitor::getStartY() const {
	return startingPoint.getY();
}

int Monitor::getEndX() const {
	return (startingPoint + dimensions).getX();
}

int Monitor::getEndY() const {
	return (startingPoint + dimensions).getY();
}

int Monitor::getWidth() const {
	return dimensions.getX();
}

int Monitor::getHeight() const {
	return dimensions.getY();
}

string Monitor::toString() const {
	stringstream stream;
	stream << name << " "
		   << ((isPrimary) ? "(primary) " : "")
		   << getWidth() << "x" << getHeight() << " "
		   << "(at " << getStartX() << ", " << getStartY() << ")";
	
	return stream.str();
}

const Coordinates& Monitor::getDimensions() const {
	return dimensions;
}
