
#include "Coordinates.h"


Coordinates::Coordinates() : x(0), y(0) {}

Coordinates::Coordinates(int x, int y) : x(x), y(y) {}

Coordinates Coordinates::operator+(const Coordinates& rhs) const {
	return Coordinates(this->x + rhs.x, this->y + rhs.y);
}

int Coordinates::getX() const {
	return x;
}

int Coordinates::getY() const {
	return y;
}
